package ejercicios;

import java.util.Random;
import java.util.Scanner;

public class ejercicio2 {

	final static int MAX = 10;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner reader = new Scanner(System.in);

		Random rnd = new Random();
		int[] vector;
		int i = 0;
		int j = MAX - 1;
		int pos = 0;
		int valor;
		boolean correcte = false;
		int minim = 0;

		vector = new int[MAX];

		while (pos < MAX) {

			vector[pos] = rnd.nextInt(100);
			System.out.print(vector[pos] + "-");
			pos++;
		}
		correcte = true;
		while (correcte == true) {
			while (j > i) {
				if (vector[j] < vector[j - 1]) {
					minim = vector[j];
					valor = vector[i];
					vector[i] = vector[j];
					vector[j] = valor;
					j--;
				} else {
					j--;
				}
				i++;
				if (i == j) {
					correcte = false;
				}
			}
		}
		pos = 0;
		System.out.println(" ");
		while (pos < MAX) {
			System.out.print(vector[pos] + "-");
			pos++;
		}
	}
}